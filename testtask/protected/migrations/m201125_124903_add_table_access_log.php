<?php

class m201125_124903_add_table_access_log extends CDbMigration
{
	public function up()
	{
        $this->createTable('tbl_access_log', array(
            "id" => "pk",
            "ip" => "varchar(255)",
            "identity" => "varchar(255)",
            "user" => "varchar(255)",
            "date" => "date",
            "time" => "time",
            "timezone" => "varchar(255)",
            "method" => "varchar(255)",
            "path" => "varchar(255)",
            "protocal" => "varchar(255)",
            "status" => "varchar(255)",
            "bytes" => "varchar(255)",
            "referer" => "varchar(255)",
            "agent" => "varchar(255)"
        ));
	}

	public function down()
	{
        $this->dropTable('tbl_access_log');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}