<?php

class m201125_122655_add_table_user extends CDbMigration
{
	public function up()
	{
        $this->createTable('tbl_user', array(
            "id" => "pk",
            "username" => "varchar(20) NOT NULL",
            "password" => "varchar(128) NOT NULL",
            "email" => "varchar(128) NOT NULL",
            "profile" => "text",
        ));
	}

	public function down()
	{
        $this->dropTable('tbl_user');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}