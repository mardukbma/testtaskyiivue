<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Yii1 TestTask',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'defaultController'=>'site',

	// application components
	'components'=>array(
		'user'=>array(
			'allowAutoLogin'=>true,
		),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=testtask',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
            // включаем профайлер
            'enableProfiling'=>true,
            // показываем значения параметров
            'enableParamLogging' => true,
		),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
            'urlManager'=>array(
                'urlFormat'=>'path',
                'rules'=>array(
                    // REST patterns
                    array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
                    array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'POST'),
                    // Other controllers
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ),
        ),
		'log'=>array(
			'class'=>'CLogRouter',

			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
                   /* 'class'=>'CProfileLogRoute',
                    'levels'=>'profile',*/
                    'enabled'=>true,

                ),
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=> require(dirname(__FILE__) . '/params.php'),
);