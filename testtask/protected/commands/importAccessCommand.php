<?php
Yii::import('application.components.AppacheLogParser');

class importAccessCommand extends CConsoleCommand
{
    public function run($args)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $apacheLogParser = new AppacheLogParser();
        $apacheLogParser->open_log_file(Yii::app()->params['acceesLogPath']);

        $data = [];
        while ($line = $apacheLogParser->get_line()) {
            $parsed_line = $apacheLogParser->format_line($line);

            if ($parsed_line && $parsed_line['ip'] && $parsed_line['date']) {
                $accessLog = Yii::app()->db->createCommand()
                    ->from('tbl_access_log')
                    ->where('ip = :ip', array(':ip' => $parsed_line['ip']))
                    ->where('date =' . date('Y-m-d', strtotime(str_replace('/', '-', $parsed_line['date']))))
                    ->where('time = :ip', array(':ip' => $parsed_line['time']))
                    ->queryRow();

                //if not exists
                if (!$accessLog) {
                    $parsed_line['date'] = date('Y-m-d', strtotime(str_replace('/', '-', $parsed_line['date'])));
                    $data[] = $parsed_line;
                }
            }
        }

        if (!empty($data)) {
            $command = $builder->createMultipleInsertCommand('tbl_access_log', $data);

            $command->execute();

            echo 'Success';
        } else {
            echo 'Not new items';
        }


    }
}