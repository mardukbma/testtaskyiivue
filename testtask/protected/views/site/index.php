<?php
if (!Yii::app()->user->isGuest) { ?>
    <div id="app">
        <form>
            <div class="row">
                <div class="form-groupp col-md-4">
                    <label>Сортровать по:</label>
                    <select v-model="sort" @change="selectEvent" class="form-control">
                        <option></option>
                        <option value="id">id</option>
                        <option value="ip">ip</option>
                        <option value="date">date</option>
                        <option value="time">time</option>
                        <option value="method">method</option>
                        <option value="path">path</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>Сначала:</label>
                    <select v-model="sortBy" @change="selectEvent" class="form-control">
                        <option value="desc">Новые</option>
                        <option value="asc">Старые</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Группировать по:</label>
                    <select v-model="groupBy" @change="selectEvent" class="form-control">
                        <option></option>
                        <option value="id">id</option>
                        <option value="ip">ip</option>
                        <option value="date">date</option>
                        <option value="time">time</option>
                        <option value="method">method</option>
                        <option value="path">path</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-groupp col-md-3">
                    <label>Поле:</label>
                    <select v-model="field" @change="selectEvent" class="form-control">
                        <option></option>
                        <option value="id">id</option>
                        <option value="ip">ip</option>
                        <option value="date">date</option>
                        <option value="time">time</option>
                        <option value="method">method</option>
                        <option value="path">path</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label>=</label>
                    <input v-model="eq" @change="selectEvent" class="form-control" type="text">
                </div>
                <div class="form-group col-md-3">
                    <label>></label>
                    <input v-model="lg" @change="selectEvent" class="form-control" type="text">
                </div>
                <div class="form-group col-md-3">
                    <label><</label>
                    <input v-model="ls" @change="selectEvent" class="form-control" type="text">
                </div>
            </div>
        </form>

        <section v-if="errored">
            <p>Ошибка загрузки</p>
        </section>

        <div v-if="loading">Loading...</div>

        <section v-else>
            <div class="row">
                <div class="col-md-2">id</div>
                <div class="col-md-2">ip</div>
                <div class="col-md-2">date</div>
                <div class="col-md-2">time</div>
                <div class="col-md-2">method</div>
                <div class="col-md-2">path</div>
            </div>
            <div v-for="item in list" class="row">
                <div class="col-md-2" v-for="(value,key) in item">
                    {{value}}
                </div>
            </div>
        </section>
    </div>
    <script>
        new Vue({
            el: '#app',
            data() {
                return {
                    list: null,
                    loading: true,
                    errored: false,
                    sort: 'id',
                    sortBy: 'desc',
                    groupBy: '',
                    field: '',
                    eq: '',
                    lg: '',
                    ls: '',
                };
            },
            mounted() {
                this.selectEvent()
            },
            methods: {
                selectEvent: function () {
                    this.loading = true;

                    axios
                        .post("<?= Yii::app()->createUrl('api/logs') ?>", {
                            "sort": this.sort,
                            "sortBy": this.sortBy,
                            "groupBy": this.groupBy,
                            "field": this.field,
                            "eq": this.eq,
                            "lg": this.lg,
                            "ls": this.ls,
                            "<?= Yii::app()->request->csrfTokenName ?>": "<?= Yii::app()->request->csrfToken ?>"
                        }, {
                            headers: {
                                "X-Asccpe-password": "<?= Yii::app()->params['apiPassword'] ?>",
                                "X-Asccpe-username": "<?= Yii::app()->params['apiLogin'] ?>"
                            }
                        })
                        .then(response => {
                            this.list = response.data;
                        })
                        .catch(error => {
                            console.log(error);
                            this.errored = true;
                        })
                        .finally(() => (this.loading = false));
                }
            }
        });
    </script>
<?php } else { ?>
    <div class="row">
        <p>Данный раздел доступен только <?php echo CHtml::link('авторизованным', 'site/login'); ?> пользователям</p>
    </div>
<?php } ?>